import { createApp } from 'vue';

import App from './App.vue';
import logggerMixing from './components/mixins/logger';

const app = createApp(App)
app.mixin(logggerMixing);
app.mount('#app');
