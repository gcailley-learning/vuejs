export default {
  addProductToCart(context, payload) {
    const productId = payload.productId;
    //search for product
    const products = context.rootGetters['products/products'];
    const product = products.find(p => p.id === productId);


    context.commit('addProductToCart', product);
  },

  removeProductFromCart(context, payload) {
    context.commit('removeProductFromCart', payload );
  }
};
