import cardMutations from './mutations';
import cardActions from './actions';
import cardGetters from './getters';

export default {
  namespaced: true,
  state() {
    return {
      items: [],
      total: 0,
      qty: 0
    };
  },
  mutations: cardMutations,
  actions: cardActions,
  getters: cardGetters
};
