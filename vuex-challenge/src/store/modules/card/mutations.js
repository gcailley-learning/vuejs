export default {
  addProductToCart(state, payload) {
    const product = payload

    const productInCartIndex = state.items.findIndex(
        ci => ci.productId === product.id
      );

    if (productInCartIndex >= 0) {
        state.items[productInCartIndex].qty++;
    } else {
      const newItem = {
        productId: product.id,
        title: product.title,
        image: product.image,
        price: product.price,
        qty: 1
      };
      state.items.push(newItem);
    }
    state.qty++;
    state.total += payload.price;
  },

  removeProductFromCart(state, payload) {
    const prodId = payload.prodId;

    const productInCartIndex = state.items.findIndex(
      cartItem => cartItem.productId === prodId
    );
    const prodData = state.items[productInCartIndex];
    state.items.splice(productInCartIndex, 1);
    state.qty -= prodData.qty;
    state.total -= prodData.price * prodData.qty;
  }
};
