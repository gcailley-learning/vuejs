export default {
  login(context) {
    context.commit('setLoggedIn', { isLoggedIn: true });
  },
  logout(context) {
    context.commit('setLoggedIn', { isLoggedIn: false });
  }
};
