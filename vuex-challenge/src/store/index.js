import { createStore } from 'vuex';

import productModule from './modules/product/index';
import cartModule from './modules/card/index';
import authModule from './modules/auth/index';

const rootStore = createStore({
  modules: {
    products: productModule,
    cart: cartModule,
    auth: authModule
  }
});

export default rootStore;
