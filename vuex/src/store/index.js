import { createStore } from 'vuex';
import counterModule  from './counter/index';
import  authModule  from './auth/index';

const rootStore = createStore({
  modules: {
    numbers: counterModule,
    auth: authModule
  }
});


export default rootStore;
