export default {
  increment(context) {
    setTimeout(() => {
      context.commit('increment');
    }, 2000);
  },
  increase(context, payload) {
    setTimeout(() => {
      console.log(context);
      console.log(payload);
      context.commit('increase', { ...payload });
    }, 2000);
  }
};
