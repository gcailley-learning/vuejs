export default {
  namespaced: true,
  state() {
    return {
      auth: false
    };
  },
  mutations: {
    setAuth(state, payload) {
      state.auth = payload.isAuth;
    }
  },

  actions: {
    login(context) {
      setTimeout(() => {
        context.commit('setAuth', { isAuth: true });
      }, 10);
    },
    logout(context) {
      setTimeout(() => {
        context.commit('setAuth', { isAuth: false });
      }, 10);
    }
  },

  getters: {
    isLoggedIn(state) {
      return state.auth;
    },
    counterXCrossStore(state, getters, rootState, rootGetters) {
      console.log(state, getters, rootState, rootGetters);
      return rootState.numbers.counter;
    }
  }
};

