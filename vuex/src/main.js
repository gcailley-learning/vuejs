import { createApp } from 'vue';
import rootStore from './store'

import App from './App.vue';


const app = createApp(App);
app.use(rootStore);
app.mount('#app');
