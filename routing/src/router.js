import { createRouter, createWebHistory } from 'vue-router';

import TeamsList from './pages/teams/TeamsList.vue';
import TeamMembers from './components/teams/TeamMembers.vue';
import TeamFooter from './pages/teams/TeamFooter.vue';
import UsersList from './pages/users/UsersList.vue';
import UsersFooter from './pages/users/UsersFooter.vue';
import NotFound from './pages/NotFound.vue';

const router = createRouter({
  history: createWebHistory(),
  routes: [
    {
      path: '/',
      redirect: '/teams'
    },
    {
      path: '/teams',
      // component: TeamsList,
      components: {
        default: TeamsList,
        footer: TeamFooter
      },
      meta: { needAuth: true },
      // alias: '/' ==> url not modified
      children: [
        {
          name: 'team-members',
          path: ':teamId',
          component: TeamMembers,
          props: true
        }
      ]
    },

    {
      path: '/users',
      components: {
        default: UsersList,
        footer: UsersFooter
      },
      beforeEnter(to, from, next) {
        console.log('User beforeEnter');
        console.log(to, from);
        next();
      }
    },
    {
      path: '/:notFound(.*)',
      // redirect: '/'
      component: NotFound
    }
  ],
  linkActiveClass: 'myactive',
  scrollBehavior(_, _2, savedPosition) {
    // console.log(to);
    // console.log(from);
    // console.log(savedPosition);
    return savedPosition
      ? savedPosition
      : {
          left: 0,
          top: 10
        };
  }
});

/**
 * Global guard.
 */
router.beforeEach(function(to, from, next) {
  console.log('Globaly');
  console.log(to);
  console.log(from);
  if (to.meta.needAuth) {
    console.log('Authentication needed');
  }
  // next(); // ==> allow
  // next(false); // ==> refuse navigation
  // if (to.query.sort) {
  //   next();
  // } else {
  //   next({name : 'team-members', params : {teamId: 't3'}, query: {sort :'asc'}})
  // }
  next();
});

/**
 * for log or stats.
 */
router.afterEach(function(to, from) {
  console.log('router.afterEach', to, from);
});

export default router;
