import { createStore } from 'vuex';
import coachesStoreModule from './modules/coaches/index';
import requestsStoreModule from './modules/requests/index';
import authModule from './modules/auth/index';

const rootStore = createStore({
  modules: {
    requests: requestsStoreModule,
    coaches: coachesStoreModule,
    auth:authModule
  }
});

export default rootStore;
