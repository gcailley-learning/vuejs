let logoutTimer;
export default {
  async auth(context, payload) {
    let url;
    if (payload.mode === 'login') {
      url =
        'https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=AIzaSyB9reaftOF1OahB1FEURIsBV5bxVi00a-s';
    } else {
      url =
        'https://identitytoolkit.googleapis.com/v1/accounts:signUp?key=AIzaSyB9reaftOF1OahB1FEURIsBV5bxVi00a-s';
    }
    const resp = await fetch(url, {
      method: 'POST',
      body: JSON.stringify({
        email: payload.email,
        password: payload.password,
        returnSecureToken: true
      })
    });
    const responseData = await resp.json();

    if (!resp.ok) {
      throw new Error(responseData.message || 'Faild to login');
    }

    const expiresIn = +responseData.expiresIn * 1000; //secondes
    const expirationDate = new Date().getTime() + expiresIn;

    localStorage.setItem('token', responseData.idToken);
    localStorage.setItem('userId', responseData.localId);
    localStorage.setItem('tokenExpiration', expirationDate);

    logoutTimer = setTimeout(() => {
      context.dispatch('autoLogout');
    }, expiresIn);

    context.commit('setUser', {
      token: responseData.idToken,
      userId: responseData.localId
    });
  },

  tryLogin(context) {
    const token = localStorage.getItem('token');
    const userId = localStorage.getItem('userId');
    const tokenExpiration = localStorage.getItem('tokenExpiration');

    if (!token && ! userId && !tokenExpiration) {
      return;
    }
    
    const expiresIn = +tokenExpiration - new Date().getTime();
    if (expiresIn > 0) {
      logoutTimer = setTimeout(() => {
        context.dispatch('autoLogout');
      }, expiresIn);
    } else {
      context.dispatch('autoLogout');
    }

    if (token && userId) {
      context.commit('setUser', {
        token: token,
        userId: userId
      });
    }
  },

  async login(context, payload) {
    return await context.dispatch('auth', { ...payload, mode: 'login' });
  },
  async signup(context, payload) {
    return await context.dispatch('auth', { ...payload, mode: 'signup' });
  },

  logout(context) {
    localStorage.removeItem('token');
    localStorage.removeItem('userId');
    localStorage.removeItem('tokenExpiration');
    clearTimeout(logoutTimer);

    context.commit('setUser', {
      token: null,
      userId: null
    });
  },

  autoLogout(context) {
    context.dispatch('logout');
    context.commit('setAutoLogout', {didAutoLogout:true});
  }
};
