export default {
  requests(state, _, _2, rootGetters) {
    const userId = rootGetters.userId;
   return state.requests.filter(r => r.coachId === userId);
  },
  hasRequests(_, getters) {
    const requests = getters.requests;
    return requests.length > 0;
  }
};
