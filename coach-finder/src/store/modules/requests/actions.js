export default {
  async contactCoach(context, payload) {
    const requestData = {
      userEmail: payload.email,
      message: payload.message
    };


    const token = context.rootGetters.token;
    const response = await fetch(
      `https://vuejs-3f722-default-rtdb.europe-west1.firebasedatabase.app/requests/${payload.coachId}.json?auth=${token}`,
      {
        method: 'POST',
        body: JSON.stringify(requestData)
      }
    );

    const responseData = await response.json();
    if (!response.ok) {
      throw new Error(responseData.message || 'Faild to contact your Coach');
    }

    context.commit('addRequest', { ...requestData, id: responseData.name });
  },

  async loadRequests(context) {
    const coachId = context.rootGetters.userId;

    const token = context.rootGetters.token;

    const response = await fetch(
      `https://vuejs-3f722-default-rtdb.europe-west1.firebasedatabase.app/requests/${coachId}.json?auth=${token}`
    );

    const responseData = await response.json();
    if (!response.ok) {
      throw new Error(responseData.message || 'Faild to fetch requests');
    }

    const requests = [];
    for (const key in responseData) {
      const request = {
        id: key,
        userEmail: responseData[key].userEmail,
        message: responseData[key].message,
        coachId : coachId
      };
      requests.push(request);
    }

    context.commit('setRequests', requests);
  }
};
