import { createApp, defineAsyncComponent } from 'vue';

import router from './router.js';
import rootStore from './store/index';
import App from './App.vue';
import BaseCard from './components/ui/BaseCard';
import BaseBadge from './components/ui/BaseBadge';
import BaseButton from './components/ui/BaseButton';
import BaseSpinner from './components/ui/BaseSpinner';
// import BaseDialog from './components/ui/BaseDialog';

// async load
const BaseDialog = defineAsyncComponent(() => import('./components/ui/BaseDialog'));

const app = createApp(App);

app.component('base-card', BaseCard);
app.component('base-badge', BaseBadge);
app.component('base-button', BaseButton);
app.component('base-spinner', BaseSpinner);
app.component('base-dialog', BaseDialog);

app.use(router);
app.use(rootStore);

app.mount('#app');
